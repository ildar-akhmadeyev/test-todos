export default {
  setPopup: (state, popupName) => (state.popup = popupName),
  setUsers: (state, users) => (state.users = users),
  setTodos: (state, todos) => (state.todos = todos),
  setActiveUserID: (state, activeId) => (state.activeUserId = activeId),
};
