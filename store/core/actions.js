import axios from 'axios';

export default {
  async loadUsers(store) {
    const users = await fetch('https://jsonplaceholder.typicode.com/users').then((response) => response.json());
    store.commit('setUsers', users);
  },
  async loadTodos(store) {
    // const todos = await fetch('https://jsonplaceholder.typicode.com/todos').then((response) => response.json());
    // store.commit('setTodos', todos);

    await axios
      .get('https://jsonplaceholder.typicode.com/todos')
      .then((response) => store.commit('setTodos', response.data));
  },
};
